package mi_hw2;

import java.util.ArrayList;
import java.util.List;

public class AudioBook {
	int id;
	List<Double> rates = new ArrayList<Double>();
	double avarageRating;
	
	AudioBook(int id, double rate){
		this.id = id;
		rates.add(rate);
	}
	
	AudioBook(int id ){
		this.id = id;
	}
	
	void rateAdder(double rate) {
		rates.add(rate);
	}
	
	void avarageRateCalculator() {
		int calculatorTemp = 0;
		int i = 0;
		for(; i < rates.size(); i++) {
			calculatorTemp += rates.get(i);
		}
		avarageRating = Double.valueOf(calculatorTemp) / Double.valueOf(i) ;
	}

}

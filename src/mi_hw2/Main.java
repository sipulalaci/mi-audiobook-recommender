package mi_hw2;

import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;


import mi_hw2.AudioBook;
import mi_hw2.People;


public class Main {
	public static void main(String[] args) throws IOException {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		String buffer;
		
		buffer = br.readLine();
		String[] line = buffer.split("\t");
		int numberOfRatings = Integer.parseInt(line[0]);
		//int numberOfUsers = Integer.parseInt(line[1]);
		//int numberOfBooks = Integer.parseInt(line[2]);
	
		ArrayList<People> people = new ArrayList<People>();
		ArrayList<AudioBook> audioBook = new ArrayList<AudioBook>();
		int actual = 1;
		boolean wasRated = false;
		boolean hasRated = false;
		while( actual <= numberOfRatings ) {
			buffer = br.readLine();
			String[] actualRating = buffer.split("\t");
			
			
			int peopleIterator = 0;
			int bookIterator = 0;
			for(People toBeTested1 : people) {
				if(toBeTested1.id == Integer.parseInt(actualRating[0])) {
					hasRated = true;
					break;
				}
				else {
					peopleIterator++;
				}
			}
			for(AudioBook toBeTested2 : audioBook) {
				//System.out.println(bookIterator);
				if(toBeTested2.id == Integer.parseInt(actualRating[1])) {
					wasRated = true;
					break;
				}
				else {
					bookIterator++;					
				}
			}
			
			if(hasRated) {
				people.get(peopleIterator).bookAdder(Integer.parseInt(actualRating[1]));
				hasRated = false;
			}
			else {
				people.add(new People(Integer.parseInt(actualRating[0]), Integer.parseInt(actualRating[1])));
			}
			if(wasRated) {
				audioBook.get(bookIterator).rateAdder(Integer.parseInt(actualRating[2]));
				wasRated = false;
			}
			else {
				audioBook.add(new AudioBook(Integer.parseInt(actualRating[1]), Integer.parseInt(actualRating[2])));
			}
			actual++;
		}	
		for(AudioBook toBeTested2 : audioBook) {
			toBeTested2.avarageRateCalculator();
		}
		//System.out.println("Avarages have been calculated!");
		
		Collections.sort(audioBook, new AvarageComparator());
		//System.out.println("Avarages have been sorted!");
		Collections.sort(people, new PeopleComparator());
			
		/*System.out.println("Test:");
		for(int i = 0 ; i < people.size(); i++) {
			for(int j = 0; j < people.get(i).ratedBookId.size(); j++) {
				System.out.println(people.get(i).ratedBookId.get(j));
			}
		}*/
		
		for(int p = 0; p < people.size(); p++) {
			outerloop:
			
			while(people.get(p).top10Recommended.size() <= 10) {
				hasRated = true;
				

				for(int i = 0; i < audioBook.size()-1; i++){
					//ArrayList<Integer> hasRated2 = new ArrayList<Integer>();
					
					int sumrate =0;
					
					for(int j = 0; j < people.get(p).ratedBookId.size(); j++) {
						if(audioBook.get(i).id == (people.get(p).ratedBookId.get(j))) {
							//hasRated2.add(1);
							sumrate ++;
						}
						else {
							//hasRated2.add(0);
						}
					}		
					//int sumrate =0;
					/*for(int j = 0; j < hasRated2.size(); j++) {
						if(hasRated2.get(j).equals(1)) {
							hasRated = true;
							break;
						}
						else {
							hasRated = false;
						}
						sumrate +=hasRated2.get(j);
					}*/
					if(sumrate == 0) hasRated = false;
					
					
					if(sumrate == 0) {
						people.get(p).top10Adder(audioBook.get(i).id);
						//System.out.println("Added!");
						hasRated = true;
					}
					if(people.get(p).top10Recommended.size() == 10) {
						break outerloop;
					}
					//hasRated2.clear();
				}
			}
		}
		/*PrintWriter pw = new PrintWriter("test.txt");
		
		
		int sum1 = 0;
		for(int p = 0; p < people.size(); p++) {
			pw.print(people.get(p).id + ":");
			for(int i = 0; i < people.get(p).ratedBookId.size(); i++){
				pw.print(" " + people.get(p).ratedBookId.get(i));
			}
			pw.println();
			sum1 += people.get(p).ratedBookId.size();
		}
		for(int p = 0; p < people.size(); p++) {
			System.out.print(people.get(p).id + ":");
			for(int i = 0; i < people.get(p).ratedBookId.size(); i++){
				System.out.print(" " + people.get(p).ratedBookId.get(i));
			}
			System.out.println();
			
		}
		PrintWriter pwk = new PrintWriter("atlagok.txt");
		int sum = 0;
		for(int k = 0; k < audioBook.size(); k++) {
			pwk.println(audioBook.get(k).id );
			System.out.println(audioBook.get(k).avarageRating);
			sum += audioBook.get(k).rates.size();
		}
		System.out.println(sum +" " +sum1);*/
		
		
		for(int i = 0; i < people.size(); i++) {
		
			//System.out.println(people.get(i).id);
			for(int j = 0; j < people.get(i).top10Recommended.size() - 1; j++ ) {
				System.out.print(people.get(i).top10Recommended.get(j) + "\t");
			}
			System.out.print(people.get(i).top10Recommended.get(people.get(i).top10Recommended.size() -1));
			System.out.println("");
			System.out.println("");
			//people.get(i).printer();
		}
	}
}
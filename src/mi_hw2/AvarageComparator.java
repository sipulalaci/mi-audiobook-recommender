package mi_hw2;

import java.util.Comparator;

public class AvarageComparator implements Comparator<AudioBook> {

	@Override
	public int compare(AudioBook ab1, AudioBook ab2) {
		
		return Double.compare(ab2.avarageRating, ab1.avarageRating);
				
	}

}

package mi_hw2;

import java.util.ArrayList;
import java.util.List;

public class People {
	int id;
	List<Integer> ratedBookId = new ArrayList<Integer>();
	
	List<Integer> top10Recommended = new ArrayList<Integer>();
	
	People(int id, int bookId){
		this.id = id;
		ratedBookId.add(bookId);
	}
	
	People(int id){
		this.id = id;
	}
	void bookAdder(int bookid) {
		ratedBookId.add(bookid);
	}
	void top10Adder(int id) {
		top10Recommended.add(id);
	}
	int getSizeOft10R() {
		return top10Recommended.size();
	}
	void printer() {
		for(int i = 0; i < top10Recommended.size(); i++) {
			if( i < top10Recommended.size() - 1 ) {
				System.out.print(top10Recommended.get(i) + "\t");
			} 
			else {
				System.out.print(top10Recommended.get(i));
			}
			System.out.println();
		}
	}
}
